
<h1>
  <a href="https://gitlab.com/songarc-games/buject"><img src="https://gitlab.com/uploads/-/system/project/avatar/26673188/buject-codebase-logo-1.png?width=88" alt="Buject Icon" width="35"></a>
  Buject
</h1>

[Table of Contents](#table-of-contents-a-to-e)

## Community Development Team
[Table of Contents](#table-of-contents-a-to-e) | 1 Company Listed

| **Company Icon** | <a href= "https://gitlab.com/songarc-games" target="_blank"><img height="36" src="https://assets.gitlab-static.net/uploads/-/system/group/avatar/11613532/dizzy.png?width=64" ></a> |
-- | -- |
| **Company Name** | SongArc Games ([@songarc-games](https://gitlab.com/songarc-games)) |

## Minimum Description
[Table of Contents](#table-of-contents-a-to-e) | 1 Item of Interest

A video game about `paying money to listen to music`.

## Median Description
[Table of Contents](#table-of-contents-a-to-e) | 1 to 3 Items of Interest

A video game about `paying money to listen to music`.

(1) You press a button to `create a music` icon in the video game

(2) You press another button to `verify that the music` should be payed for and applied

(3) You listen to music that `changes as you pay` for it and even once in a while in the background on its own

## Maximum Description
[Table of Contents](#table-of-contents-a-to-e) | 1 to 9 Items of Interest

<details>
<summary>(1) A Note About Games</summary>

### (1) A Note About Games

A game is an interesting structure. It is able to create new games if it is intended to do so. A video game without games can possibly be a plain video game that doesn't have interesting dynamics like **(1)** `fast-forward correction` and **(2)** `correction without stigmas` and **(3)** `informational related correction schemes` and finally **(4)** `information streams without a primary source`.

A game by itself can mean nothing without players. Even this statement can be useless without a reader.

A reader is useful. Each reader is unique and capable of handicapping the game to an extreme extent.

Concept art is a great place to start to create an interesting game.

</details>


<details>
<summary>(2) A Note About Theory Of Games</summary>

### (2) A Note About Theory Of Games

</details>

<details>
<summary>(3) A Note About Music</summary>

### (3) A Note About Music

</details>

<details>
<summary>(4) Music</summary>

### (4) Music

</details>

## Projects That Consume This Resource
[Table of Contents](#table-of-contents-a-to-e) | 0 Consumers Listed (and 0 Discovered Since May 15, 2021; <mark>[Let Us Know](https://gitlab.com/songarc-games/buject/-/issues) If You Use The <a href="">JavaScript Library</a> To Be Included In This Consumer Listing Resource</mark>)

## Projects That Provide A Related Usecase
[Table of Contents](#table-of-contents-a-to-e) | 1 Company Listed

| **Provider Icon** | <a href= "https://gitlab.com/songarc-games" target="_blank"><img height="36" src="https://assets.gitlab-static.net/uploads/-/system/group/avatar/11613532/dizzy.png?width=64" ></a> |
| -- | -- |
| **Provider Name** | SongArc Games ([@songarc-games](https://gitlab.com/songarc-games)) |
| **Provider Product Listing** | (1) [buject](https://gitlab.com/songarc-games/buject) (same usecase) |

## What To Expect From This Community Document
[Table of Contents](#table-of-contents-a-to-e)

### Table of Contents (A)

- (A) Project Inspiration (3 Areas of Interest)

## (A) Project Inspiration
[Table of Contents](#table-of-contents-a-to-e) | 1 to 3 Items of Interest

<details>
<summary>(A.1) Creating A Minimum Game Idea - In Theory</summary>

### (A.1) Creating A Minimum Game Idea - In Theory

A game can be fun to work on. But sometimes the game can take a lot of time to make. If your game is really small and minimal, what would that look like in a nice '2-dimensional' environment where your player character stands still and performs actions?

A video game that is minimal could possibly look like `a 3D adventure game where new characters emerge` from a distant background and approach the player with a `constant speed`.

A `constant speed` is useful for (1) keeping the player's attention on the changes to the environment.

A `constant speed` is useful for (A) keeping the player's attention engaged on new and dynamic forces based on features like 'proportionality' instead of possibly engaging in discontent because the proportions are not engaging enough to warrant a full play through.

A `constant speed` is useful for (Alpha) it is interesting to watch a player react to an environment without necessarily knowing what the constants are.

A `constant speed` is useful for (I) it is useful to update software.

</details>

<details>
<summary>(A.2) Making Money From The Game</summary>

### (A.2) Making Money From The Game

A player can make a lot of money in a video game by selling resources. Buject is a game where the player has to sell their time to play well.

If the player sells their time effectively, they will get a jolt of mind melding music that is fun to listen to.

</details>

<details>
<summary>(A.3) Doing A Resource Exchange That Is Cheap And Affordable</summary>

### (A.3) Doing A Resource Exchange That Is Cheap And Affordable

A game is useful for individuals to exchange resources. A player providing commentary like 'that was fun' and 'that was intense' is a good measure of what a possible good trade was for their 'time'. Thank you for playing 'Buject'. Thank you so much!

</details>

## Thank You

Thanks for reading this document. I hope you got something out of it.

