
# Tornadoes

We can use tornadoes as a symbol for 'hurricanes' or 'wet water expeditions' where we want the user to follow the idea of a 'large extreme event'

Social events like tornadoes are supposed to be enjoyable to watch if you are sure of safety and can walk out of harm's way.

If you are afraid of the damage that a 'tornado' can do, then you may not like the event.

Tornadoes can be 'blue' in the media art for some of the photographs

. . 

`Blue Tornado Emoji Icon`

. . 



