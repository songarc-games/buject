

# How Do You Retain An Audience - 1 - Sex

# Update #2 - May 16, 2021

### [Sunstrider Striderson][12:30PM]:

Making a color wheel for a video game . . might be a unique process . . I'm not really sure how to go about . . developing the color

. . 

I guess I'll color around using a paint brush to see what comes of that process . . the colors and objects aren't necessarily clear to me . . although the idea of 'spheres' is in my mind as in-game items that are used . . 

. . 

### [Sunstrider Striderson][14:47PM]:

Concept Art Photograph #1
![Concept Art #1](../../../service-for-concept-art/concept-art-type-1-interesting-first-ideas/interesting-first-ideas-type-1-2021-16-05-concept-art/concept-art-1.png)

Concept Art Photograph #2
![Concept Art #2](../../../service-for-concept-art/concept-art-type-1-interesting-first-ideas/interesting-first-ideas-type-1-2021-16-05-concept-art/concept-art-2.png)

Concept Art Photograph #3
![Concept Art #3](../../../service-for-concept-art/concept-art-type-1-interesting-first-ideas/interesting-first-ideas-type-1-2021-16-05-concept-art/concept-art-3.png)

Concept Art Photograph #4
![Concept Art #4](../../../service-for-concept-art/concept-art-type-1-interesting-first-ideas/interesting-first-ideas-type-1-2021-16-05-concept-art/concept-art-4.png)



# Update #1 - May 15, 2021

### [Sunstrider Striderson][10:59AM]:

I don't know why but maybe 'sex' has to do with (1) `social stigma` and (2) `interesting ideas like heart throbbing love and maybe some other things`.

### [Sunstrider Striderson][10:59AM]:

I think this game can have 'sex' elements without having to 'spell' those out. `No Nudity`. `No Pornography`. No explicit ideas of 'sexual related content' but the idea is that you can have 'sex' by 'giving all your money away'. 'Giving all your money away' is a core feature of the game. '`You can give all your money away`' and '`live a good life`'. 

### [Sunstrider Striderson][11:01AM]:

Forgive me for saying this but this is weird. Especially in a social context with fear around gaming like (1) how many people can you sleep with in your life time? (2) How many people can you steal their first kiss or their first vagina penetration experience? (3) How many people can you forget in the course of doing all of that? (4) Forgive me again but finally you have an extreme case like 'how many children can you have with the person and not have to pay for their school or education or look after them ever again?' 'can't they just look after themselves?' 'seriously?' 'why do I have to do all the manual work around here?' 'lame' 'annoying' . . These are all extreme usecases for a type of pornography that can be addicting to people. Men especially like this lifestyle and so they will want to fornicate with a person without looking after them because it is assumed 'they can take care of themselves' . .

### [Sunstrider Striderson][11:06AM]:

It is an extreme example of stupidity to write this message. I'm sorry. I should be banned from the internet.


### [Sunstrider Striderson][11:09AM]:

Marketing Ideas:

- Rumble Racing
- Zone of the Enders
- 


A Car streak game can be like a sexy fast game idea that is inspiring and insightful.

A robot fighter pilot game can be like a sexy interesting game that doesn't respond to text messages from fighter pilots. You have to fight with Ada who assists you to make every purchase.

. . 

. . 

"Make your move!"

"Pay it up!"

"Fight harder"

"Fight to win"

"Only pay what you must"

"Enough with the cheap moves"

"Keep fighting"

"Alright you're out of steam"

"Make a left turn"

"Don't go too far ahead of yourself"

"You're the best!"

"You're amazing!"

"You're on fire!"

"You keep asking for advice"

"When will you take a break already?"

"Alright, let's rewind"

"Nice play"

"Don't give up!"

"Stop giving up!"

"Stop it already!"

"You can't keep asking for permission, take it you lunatic!"

"Don't keep me waiting"

"Don't take me for granted"

"Alright, now's not a good time"

"Alright, fight me"

"Fight me with more enthusiasm"

"Alright, that's a good game"


### [Sunstrider Striderson][11:19AM]:

I don't know why but "`yellow and red`" could possibly be good color combinations . . 

Yellow and Red remind me of "racing car games like Rumble Racing" and could be "fast" and "action" oriented . . 

A yellow and red speed racer game . . 

. . 

I think blue and purple could be other color combinations that could possibly help the game charge "contexts" . . a context could be like a special mood in the game like "midnight" . . 

. . 

`Yellow-Red`

`Blue-Purple`

`Green-Emerald`

. . 

A blue sky for a skybox could work well for the first few minutes of the game . . any special rewards could transfer the game to a midnight color . . any really special ideas could transfer the game to a 'geen-emerald' idea . . 

. . 

Buject looks like a multiplayer game . . but it's a single player game . . I think the advertisement should include . . flashes of other players playing the game . . 

. . 




