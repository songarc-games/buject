
let nodejsFileSystemLibrary = require('fs')
let nodejsFileSystemPathLibrary = require('path')

// console.log('Starting Computer Program: Codebase\n')

const sourceDirectoryNameForScanning = process.argv[2]
const destinationDirectoryNameForCodebaseJsonPublishing = process.argv[3]
const sourceHttpUrlForDirectoryBaseUrl = process.argv[4]

// Please use "double-quotes" instead of 'single-quotes' for the 'file or folder name list' items
// For example '[".gitignore","hello-world-123.png"]'
const ignoreSourceFileOrFolderList = JSON.parse(process.argv[5] || '[]')
const ignoreSourceFileReferenceList = JSON.parse(process.argv[6] || '[]')



createCodebaseJsonFile()

// create codebase.json file
function createCodebaseJsonFile () {
    let ignoreFileContentItemTable = {}

    // Add files to ignore file content item table
    for (let i = 0; i < ignoreSourceFileOrFolderList.length; i++) {
        const fileOrFolderName = ignoreSourceFileOrFolderList[i]
        ignoreFileContentItemTable[fileOrFolderName] = true
    }

    // Add reference files to ignore file content item table
    for (let i = 0; i < ignoreSourceFileReferenceList.length; i++) {
        const fileName = ignoreSourceFileReferenceList[i]
        const contentItemTable = getIgnoreFileContentItemTable(sourceDirectoryNameForScanning, fileName)
        ignoreFileContentItemTable = Object.assign(ignoreFileContentItemTable, contentItemTable)
    }

    const fileSystemDataStructureResult = getFileSystemDataStructure(sourceDirectoryNameForScanning, sourceHttpUrlForDirectoryBaseUrl, ignoreFileContentItemTable)
    // console.log(fileSystemDataStructureResult.numberOfAncestryFiles)
    // console.log(fileSystemDataStructureResult.dataStructureList)

    const codebaseJsonFile = JSON.stringify({
        fileName: 'codebase.json',
        baseWebsiteUrl: sourceHttpUrlForDirectoryBaseUrl,
        numberOfAncestryFiles: fileSystemDataStructureResult.numberOfAncestryFiles,
        codebaseDataStructureList: fileSystemDataStructureResult.dataStructureList
    }, null, 4)

    createFileSystemFile(destinationDirectoryNameForCodebaseJsonPublishing, 'codebase.json', codebaseJsonFile)
}


function getIgnoreFileContentItemTable (directoryName, fileName) {
    const directoryAndFileName = nodejsFileSystemPathLibrary.join(directoryName + '/' + fileName)
    const readFileContent = nodejsFileSystemLibrary.readFileSync(directoryAndFileName, { encoding : 'utf8' })

    const fileContentItemTable = {}

    // .gitignore files are supported by this
    let fileContent = removeCommentLineFromTextString(readFileContent, '#')

    const fileContentItemList = fileContent.split('\n').filter(itemName => itemName ? true : false)
    for (let i = 0; i < fileContentItemList.length; i++) {
        const fileOrFolderName = fileContentItemList[i]
        fileContentItemTable[fileOrFolderName] = true
    }

    // other file types can be considered here
    // fileContent = removeCommentLineFromTextString(fileContent, '//')
    // fileContent = removeCommentLineFromTextString(fileContent, ['/*', '*/'])

    return fileContentItemTable
}

function getFileSystemDataStructure (directoryName, baseDirectoryHttpUrl, ignoreFileOrFolderTable) {
    let dataStructureList = []
    let numberOfAncestryFiles = 0

    // get the file or folder name list for the local directory name
    let localDirectoryStructureList = nodejsFileSystemLibrary.readdirSync(directoryName)

    // traverse the directory listing
    localDirectoryStructureList.forEach(function (fileOrFolderName) {
        fileOrFolderName = nodejsFileSystemPathLibrary.join(directoryName + '/' + fileOrFolderName)

        // ignore certain files and folders
        if (ignoreFileOrFolderTable[fileOrFolderName]) {
            return
        }

        // ignore pattern-matching files and folders

        let fileOrFolderStatistics = nodejsFileSystemLibrary.statSync(fileOrFolderName)

        // get more directories that are children or nested relatives of this local directory
        if (fileOrFolderStatistics && fileOrFolderStatistics.isDirectory()) { 
            const fileSystemDataStructureResult = getFileSystemDataStructure(fileOrFolderName, baseDirectoryHttpUrl, ignoreFileOrFolderTable)
            
            dataStructureList.push({
                directoryName: fileOrFolderName,
                directoryHTTPURL: `${baseDirectoryHttpUrl}/${fileOrFolderName}`,
                numberOfAncestryFiles: fileSystemDataStructureResult.numberOfAncestryFiles,
                directoryAncestryList: fileSystemDataStructureResult.dataStructureList
            })

            numberOfAncestryFiles += fileSystemDataStructureResult.numberOfAncestryFiles
        
        // list the file in the data structure list
        } else {
            dataStructureList.push({
                fileName: fileOrFolderName,
                directoryHTTPURL: `${baseDirectoryHttpUrl}/${fileOrFolderName}`
            })

            numberOfAncestryFiles += 1
        }
    })

    // return the data structure list
    return {
        dataStructureList,
        numberOfAncestryFiles
    }
}

function createFileSystemFile (destinationDirectoryName, fileName, fileContent) {
    const directoryAndFileName = nodejsFileSystemPathLibrary.join(destinationDirectoryName + '/' + fileName)

    nodejsFileSystemLibrary.writeFile(directoryAndFileName, fileContent, { encoding : 'utf8' }, writeFileError => {
        if (writeFileError) {
            console.error(writeFileError)
            return
        }
        console.log('File created successfully!')
    })
}

function updateFileSystemDataStructure () {
    //
}

function getIndexListForListItem (list, listItem) {
    let indexList = []
    let indexNumber = list.indexOf(listItem)

    while (indexNumber != -1) {
        indexList.push(indexNumber)
        indexNumber = list.indexOf(listItem, indexNumber + 1)
    }

    return indexList
}

function removeCommentLineFromTextString (textString, characterStartAndEndList) {

    const isString = typeof characterStartAndEndList === 'string'
    
    const commentStartCharacter = isString ? characterStartAndEndList : characterStartAndEndList[0]
    const commentEndCharacter = isString ? '\n' : (characterStartAndEndList[1] || '\n')

    const commentStartIndexList = getIndexListForListItem(textString, commentStartCharacter)

    const intermediateDeleteConstant = '@?@~!'

    const textStringCharacterList = textString.split('')

    for (let i = 0; i < commentStartIndexList.length; i++) {

        const commentStartIndex = commentStartIndexList[i]
        let numberOfCharactersSinceStartIndex = 0

        // Search for the first '/n' (new line character) before removing that content
        for (let j = commentStartIndex; j < textStringCharacterList.length; j++) {
            const isNewLineCharacter =  (textStringCharacterList[j] === commentEndCharacter)
            numberOfCharactersSinceStartIndex += 1

            if (!isNewLineCharacter) {
                continue
            }

            const commendEndIndex = commentStartIndex + numberOfCharactersSinceStartIndex
            const fileContentCommentList = textStringCharacterList.slice(commentStartIndex, commendEndIndex)
            const fileContentIntermediateDeleteCommentList = fileContentCommentList.map(() => intermediateDeleteConstant)

            // Add intermediate delete constant
            textStringCharacterList.splice(commentStartIndex, numberOfCharactersSinceStartIndex, ...fileContentIntermediateDeleteCommentList)

            break
        }
    }

    // Delete the intermediate delete constants
    for (let i = 0; i < textStringCharacterList.length; i++) {
        const isIntermediateDeleteConstant = (textStringCharacterList[i] === intermediateDeleteConstant)
        if (!isIntermediateDeleteConstant) {
            continue
        }

        textStringCharacterList[i] = ''
    }

    return textStringCharacterList.join('')
}

exports.getFileSystemDataStructure = getFileSystemDataStructure
exports.createCodebaseJsonFile = createCodebaseJsonFile
exports.updateFileSystemDataStructure = updateFileSystemDataStructure
exports.removeCommentLineFromTextString = removeCommentLineFromTextString

